<?php

namespace App\Repositories;


use App\Task;
use App\User;

class TaskRepository
{
    /**
     * @param User $user
     * @return collection
     */
    public function forUser(User $user)
    {
        return Task::where('user_id', $user->id)
            ->orderBy('created_at', 'asc')
            ->get();
    }
}