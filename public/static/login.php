<?php require_once('header.php') ?>
<div class="row">
    <div class="col-md-4 col-md-offset-4 floating-box mt-100">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">请登录</h3>
            </div>
            <div class="panel-body">
                <form method="POST" action="https://laravel-china.org/auth/login" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="KmcAaUGQobFXed8BbNTQRQqS2o1nq1v5BBA090Gf">
                    <div class="form-group ">
                        <label class="control-label" for="email">邮 箱</label>
                        <input class="form-control" name="email" type="text" value="" placeholder="请填写 Email">
                    </div>
                    <div class="form-group ">
                        <label class="control-label" for="password">密 码</label>
                        <input class="form-control" name="password" type="password" value="" placeholder="请填写密码">
                    </div>
                    <button type="submit" class="btn btn-success btn-block">
                        <i class="fa fa-btn fa-sign-in"></i> 登录
                    </button>
                </form>
                <p class="text-right small">
                    <span>没有账号？</span>
                    <a href="register.php" class="lh-2">立即注册</a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php require_once('footer.php') ?>
