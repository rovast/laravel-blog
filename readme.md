基于laravel5.1编写的博客，主要用于熟练laravel，同时也当是给新手学习参考。

主要功能：
- 注册[done]
- 登录[done]
- 添加任务[done]
- 任务列表[done]
- 删除任务[done]
- 忘记密码[doing]
- 文章发布[coming soon]
- 显示[coming soon]
