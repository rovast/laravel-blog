@extends('layouts.master')

@section('title')
    登录
@endsection

@section('content')
    @include('errors.top-alert')
    <div class="row">
        <div class="col-md-4 col-md-offset-4 floating-box mt-50">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">请登录</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open() !!}
                        <div class="form-group">
                            {!! Form::label('email','Email') !!}
                            {!! Form::email('email','',['class' => 'form-control' , 'placeholder' => 'Your email']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email','Password') !!}
                            {!! Form::password('password',['class' => 'form-control' , 'placeholder' => 'Your password']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('登录',['class' => 'btn btn-success btn-block']) !!}
                        </div>
                    {!! Form::close() !!}

                    <p class="text-right small">
                        <span>没有账号？</span>
                        <a href="{{ url('auth/register') }}" class="lh-2">立即注册</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection