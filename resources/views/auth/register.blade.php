@extends('layouts.master')

@section('title')
    注册
@endsection

@section('content')
    @include('errors.top-alert')
    <div class="row">
        <div class="col-md-4 col-md-offset-4 floating-box mt-50">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">请注册</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open() !!}
                    <div class="form-group">
                        {!! Form::label('name','Name') !!}
                        {!! Form::text('name','',['class' => 'form-control' , 'placeholder' => 'Your name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email','Email') !!}
                        {!! Form::email('email','',['class' => 'form-control' , 'placeholder' => 'Your email']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('password','Password') !!}
                        {!! Form::password('password',['class' => 'form-control' , 'placeholder' => 'Your password']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('password_confirmation','Password') !!}
                        {!! Form::password('password_confirmation',['class' => 'form-control' , 'placeholder' => 'Confirm your password']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('注册',['class' => 'btn btn-success btn-block']) !!}
                    </div>
                    {!! Form::close() !!}

                    <p class="text-right small">
                        <span>已有账号？</span>
                        <a href="{{ url('auth/login') }}" class="lh-2">立即登录</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection