<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>@section('title') Blog @show - laravel5.1</title>
    <link rel="stylesheet" type="text/css" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">Home</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">文章</a></li>
                    <li><a href="{{ url('task/all') }}">全部任务</a></li>
                    <li><a href="{{ url('task') }}">我的任务</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                               aria-expanded="false">{{ Auth::User()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">+文章</a></li>
                                <li><a href="{{ url('task/create') }}"> + 任务</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ url('auth/logout') }}">退出</a></li>
                            </ul>
                        </li>
                    @else
                        <li><a href="{{ url('auth/login') }}">登录</a></li>
                        <li><a href="{{ url('auth/register') }}">注册</a></li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <div class="container">
        @yield('content')
    </div>
    <script type="text/javascript" src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>
