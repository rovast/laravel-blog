@extends('layouts.master')

@section('title')
	添加任务
@endsection

@section('content')
    @include('errors.top-alert')
    {!! Form::open(['url' => url('task')]) !!}
    <div class="form-group">
        {!! Form::label('name','Task Name') !!}
        {!! Form::text('name','',['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('添加任务',['class' => 'btn btn-success btn-block']) !!}
    </div>
    {!! Form::close() !!}
@endsection