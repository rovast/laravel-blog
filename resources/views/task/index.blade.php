@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">我的任务</div>
            <div class="panel-body">
                @if(count($tasks))
                    @foreach($tasks as $task)
                        <div class="clearfix">
                            <span class="h4">{{ $task->name }}</span>
                            <label class="label label-default small">{{ $task->user->name }}</label>
                            <label class="label label-primary small">{{ $task->updated_at->diffForHumans() }}</label>
                            {!! Form::open(['url' => 'task/'.$task->id , 'method' => 'delete']) !!}
                            {!! Form::submit('删除',['class' => 'btn btn-danger pull-right']) !!}
                            {!! Form::close() !!}
                        </div>
                        <hr>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection